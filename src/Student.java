import java.util.ArrayList;
import java.util.Scanner;
import java.util.stream.Stream;

public class Student extends Person implements Comparable{
    private static Student Students;
    private int studentId;
    private int score;



    public Student(){
        super();
    }

    protected Student(String name, String lastName, int studentId, int score){
        super(name,lastName);
        this.studentId = studentId;
        this.score = score;
    }
    public int getStudentId(){
        return this.studentId;
    }
    public void setStudentId(int studentId){
        this.studentId = studentId;
    }
    public int getScore() {
        return score ;
    }
    public void setScore(int score) {
        this.score = score;
    }


    public void addStudent (ArrayList<Student>students , Scanner input){
        System.out.println("Inter the name");
        String name = input.next();
        System.out.println("Inter the lastName");
        String lastName = input.next();
        System.out.println("Inter the student ID");
        int studentId = input.nextInt();
        System.out.println("Inter the score");
        int score = input.nextInt();
        Student S = new Student(name, lastName,studentId,score);
        students.add(S);
        System.out.println("Successful");
    }

    public static Student listOfStudent(ArrayList<Student> students){
        for (int i = 0 ; i < students.size(); i++) {
            Student S = new Student(students.get(i).getName(),students.get(i).getLastName(),students.get(i).getStudentId(),students.get(i).getScore());
            System.out.println("Code :" + i + " " + S );
        }
        return Students;
    }
    public static void listStudentPass(ArrayList<Student>students) {
        Stream<Student> filter= students.stream().filter(s -> s.score > 12);
        filter.forEach(student -> System.out.println(student.getName()+": "+student.score));
    }
    @Override
    public int getId() {
        return this.studentId ;
    }

    @Override
    public int compareTo(Object o) {
        return super.compareTo((Person)o);
    }

}
