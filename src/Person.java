public abstract class Person implements Comparable{
    private String name;
    private String lastName;

    protected Person() {
    }
    protected Person(String name, String lastName){
        this.name = name;
        this.lastName = lastName;
    }
    public String getName (){
        return this.name;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getLastName (){
        return this.lastName;
    }
    public void setLastName(String lastName){
        this.lastName = lastName;
    }

    public int compareTo(Person o){
        return name.compareTo(o.name);
    }

    public String toString(){
        return "Name: " + this.name + "\tLastName: " + this.lastName + "\t ID: " + getId() + "\t Score: " + getScore();
    }
    public abstract int getId();
    public abstract int getScore();

}
