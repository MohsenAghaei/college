import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;

public class Teacher extends Person implements Comparable {
    private int teacherId;
    private Student StudentId;
    private static Teacher Teachers;


    public Teacher(){
        super();
    }
    protected Teacher(String name, String lastName, int teacherId){
        super(name, lastName);
        this.teacherId = teacherId;
    }
    public int getTeacherId(){
        return this.teacherId;
    }
    public void setTeacherId(int teacherId){
        this.teacherId = teacherId;
    }
    public Student getStudent(){
        return this.StudentId;
    }
    public void setStudents(Student students){
        this.StudentId = students;
    }
    public void addTeacher (ArrayList<Teacher> teachers , Scanner input){
        System.out.println("Inter the name");
        String name = input.next();
        System.out.println("Inter the lastName");
        String lastName = input.next();
        System.out.println("Inter the Teacher ID");
        int studentId = input.nextInt();
        Teacher T = new Teacher(name, lastName,studentId);
        teachers.add(T);
        System.out.println("Successful");
    }

    public void addStudentsToListTeacher(Map<Integer,Student> LSP ,ArrayList<Teacher>teachers, ArrayList<Student>students,Scanner input){
        System.out.print("Which Teacher:\n");
        listOfTeacher(teachers);
        System.out.println("Inter the Code Teacher");
        int CodeT = input.nextInt();
        Teacher T = new Teacher(teachers.get(CodeT).getName(),teachers.get(CodeT).getLastName(),teachers.get(CodeT).getId());

        System.out.print("Which Student:\n");
        Student.listOfStudent(students);
        System.out.println("Inter the Code Student");
        int CodeS = input.nextInt();
        Student S = new Student(students.get(CodeS).getName(),students.get(CodeS).getLastName(),students.get(CodeS).getId(),students.get(CodeS).getScore());

        LSP.put(T.teacherId,S);
        for(Map.Entry<Integer,Student> entry: LSP.entrySet()){
            System.out.println("Teacher :" + entry.getKey()+ "\nStudents:" + entry.getValue());
        }
        System.out.format("Student. %s with Code. %s added to the list %s \n",S.getLastName(),S.getId(),T.getLastName());
    }
    public static void StudentfromthelistTeacher(Map<Integer, Student> LSP, ArrayList<Teacher> teachers, Scanner input) {
        listOfTeacher(teachers);
        System.out.println("Inter the Teacher ID");
        int studentId = input.nextInt();
        System.out.println(LSP.get(studentId));

    }
    /*
    public void changeScoreStudents(Map<Integer,Student> LSP,ArrayList<Teacher>teachers,ArrayList<Student>students,Scanner input){
        listOfTeacher(teachers);
        System.out.println("Inter the Teacher ID");
        int studentId = input.nextInt();

        Student.listOfStudent(students);
        System.out.println("Inter the Teacher ID");
        int score = input.nextInt();

        for(Map.Entry<Integer,Student> entry: LSP.entrySet()) {
            System.out.println("Teacher :" + entry.getKey() + "\n Students:" + entry.setValue(new Student(null,null,setScore(score),null)));
        }
           LSP.replace(studentId,getScore(),score);
    }
     */

    public int getId(){
        return this.teacherId ;
    }
    @Override
    public int compareTo(Object o) {
        return super.compareTo((Person)o);
    }

    public static Teacher listOfTeacher(ArrayList<Teacher> teachers){
        for (int i = 0 ; i < teachers.size(); i++) {
            Teacher T = new Teacher(teachers.get(i).getName(),teachers.get(i).getLastName(),teachers.get(i).teacherId);
            System.out.println("Code :" + i +"\t " + T);
        }
        return Teachers;
    }
    public int getScore() {
        return 0;
    }

}
