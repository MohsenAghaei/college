
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

import java.util.Hashtable;
import java.util.Map;
import java.util.Scanner;

public class main {
    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {

        Student Students = new Student();
        Teacher Teachers = new Teacher();

        ArrayList<Teacher> teachers = new ArrayList<Teacher>();
        ArrayList<Student> students = new ArrayList<Student>();

        Map<Integer,Student> LSP = new Hashtable<Integer, Student>();

        File fStudent = new File("C:\\Users\\mohsen\\College\\src\\Student.txt");
        try {
            Scanner fileSudent = new Scanner(fStudent);
            while (fileSudent.hasNext()) {
                String line = fileSudent.next();
                String[] lineSplited = line.split(" : ");
                String name = lineSplited[1];
                String lastname = lineSplited[2];
                int code = Integer.parseInt(lineSplited[3]);
                int score = Integer.parseInt(lineSplited[4]);
                Student S = new Student(name,lastname,code,score);
                students.add(S);
            }
        } catch (Exception e) {
            System.out.println(" java.io.FileNotFoundException.0");
        }

        File fTeacher = new File("C:\\Users\\mohsen\\College\\src\\Teacher.txt");
        try {
            Scanner fileTeacher = new Scanner(fTeacher);
            while (fileTeacher.hasNext()) {
                String line = fileTeacher.next();
                String[] lineSplited = line.split(" : ");
                String name = lineSplited[1];
                String lastname = lineSplited[2];
                int code = Integer.parseInt(lineSplited[3]);
                Teacher T = new Teacher(name, lastname, code);
                teachers.add(T);
            }
        }catch(Exception e){
                System.out.println(" java.io.FileNotFoundException.1");
        }

        File fileStudentOfTeacher = new File("C:\\Users\\mohsen\\College\\src\\fileStudentOfTeacher.txt");
        try {
            Scanner fileStT = new Scanner(fileStudentOfTeacher);
            while (fileStT.hasNext()) {
                String line = fileStT.next();
                String[] lineSplited = line.split(" : ");
                int Id =Integer.parseInt(lineSplited[1]);
                String name = lineSplited[2];
                String lastname = lineSplited[3];
                int code = Integer.parseInt(lineSplited[4]);
                int score = Integer.parseInt(lineSplited[5]);
                Student S = new Student(name,lastname,code,score);
                students.add(S);
                LSP.put(Teachers.getId(),S);
            }
        } catch (Exception e) {
            System.out.println(" java.io.FileNotFoundException.2");
        }

        while (true) {
            System.out.println("0.Exit || 1.Student || 2.Teacher");
            String Ans = input.next();
            if (Ans.equals("1")) {
                System.out.println(" 1.Add Student || 2.List Student || 3.list Student Passed");
                String ans = input.next();
                if (ans.equals("1")) {
                    Students.addStudent(students, input);
                    saveListStudent(students);
                } else if (ans.equals("2")) {
                    Students.listOfStudent(students);
                } else if (ans.equals("3")) {
                    Student.listStudentPass(students);
                }
            } else if (Ans.equals("2")) {
                System.out.println("1.Add Teacher || 2.List Teachers || 3.Add a Student to the list Teacher || 4.Search the Student from the list Teacher  ");
                String ans = input.next();
                if (ans.equals("1")) {
                    Teachers.addTeacher(teachers, input);
                    saveListTeacher(teachers);
                } else if (ans.equals("2")) {
                    Teachers.listOfTeacher(teachers);
                } else if (ans.equals("3")) {
                    Teachers.addStudentsToListTeacher(LSP,teachers,students,input);
                    saveListStudentOfTeacher(LSP,teachers,students);
                } else if (ans.equals("4")) {
                    Teacher.StudentfromthelistTeacher(LSP,teachers,input);
                    saveListStudentOfTeacher(LSP,teachers,students);
              //  }else if (ans.equals("5")) {
              //      Teachers.listStudentPass(LSP,teachers,input); // TODO: 10/17/2020 Change value field Score
                }

            } else if (Ans.equals("0")) {
                break;
            }

        }

    }
    private static void saveListStudent(ArrayList<Student>students) {
        try {
            File f = new File("C:\\Users\\mohsen\\College\\src\\Student.txt");
            FileWriter fw = new FileWriter(f);
            for (int i = 0; i < students.size(); i++) {
                fw.write(students.get(i).getName()+ ":" + students.get(i).getLastName() + ":" + students.get(i).getId() + ":" + students.get(i).getScore() + ":" + "\n");
            }
            fw.close();
        } catch (Exception e) {
            System.out.println("java.io.IOException");
        }
    }

    private static void saveListTeacher(ArrayList<Teacher>teachers) {
        try {
            File f = new File("C:\\Users\\mohsen\\College\\src\\Teacher.txt");
            FileWriter fw = new FileWriter(f);
            for (int i = 0; i < teachers.size(); i++) {
                fw.write(teachers.get(i).getName()+ ":" + teachers.get(i).getLastName() + ":" + teachers.get(i).getId() + ":" + "\n");
            }
            fw.close();
        } catch (Exception e) {
            System.out.println("java.io.IOException");
        }
    }
    private static void saveListStudentOfTeacher(Map<Integer,Student>LSP,ArrayList<Teacher>teachers,ArrayList<Student>students) {
        try {
            File f = new File("C:\\Users\\mohsen\\College\\src\\fileStudentOfTeacher.txt");
            FileWriter fw = new FileWriter(f);
            for (Map.Entry<Integer,Student> entry:LSP.entrySet()){
                fw.write(entry.getKey() + " : " +entry.getValue()+ " : " + "\n");
            }
            fw.close();
        } catch (Exception e) {
            System.out.println("java.io.IOException");
        }
    }

}
